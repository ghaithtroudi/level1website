<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('CREATE VIEW physical_person as 
            SELECT id, email, `password`, cin, lastname, name, address, tel, occupation, birthday, `type`, 
            created_at, updated_at, remember_token
            FROM clients
            WHERE `type`="Individu";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('DROP VIEW personne_physiques');
    }
}
