<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function(Blueprint $table) {

            $expireDate = (new DateTime())->add(new DateInterval('P15D'))->format('Y-m-d H:i:s');

            $table->increments('id');
            $table->string('email', 50)->unique();
            $table->string('password')->nullable();
            $table->string('cin', 8)->nullable();
            $table->string('company_name')->nullable();
            $table->string('matricule')->nullable();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('tel')->nullable();
            $table->string('address')->nullable();
            $table->string('occupation')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('type', resolve('client-types'))->default('Individual');
            $table->enum('provider', resolve('auth-providers'))->default('email')->nullable();
            $table->string('provider_id')->nullable();
            $table->dateTime('expireDate')->default($expireDate);
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
