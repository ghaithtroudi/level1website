<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('clients')->truncate();
        $client = new Client([
            'email' => 'test@test.com',
            'password' => password_hash('123456', CRYPT_BLOWFISH),
            'provider' => 'email',
            'expireDate' => (new DateTime())->format('Y-m-d H:i:s'),
        ]);
        $client->save();
    }
}
