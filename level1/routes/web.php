
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('index');});

// client login
Route::get('/login', 'AuthController@getLogin')->name('getClientLogin');
Route::get('/register', 'AuthController@getRegister')->name('getClientRegister');
Route::get('/logout', 'AuthController@logout')->name('clientLogout');
Route::post('/login/{provider}', 'AuthController@login');
Route::get('/login/{provider}/callback', 'AuthController@loginCallback');
Route::post('/register/{provider}', 'AuthController@register');


// client testing 
Route::get('/client', 'ClientController@index');


// client password reset
Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::match(['get', 'head'],'/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'ResetPasswordController@reset');
Route::post('/password/reset', 'ResetPasswordController@showResetForm')->name('password.reset');

// page to return to after password reset
Route::get('/home', 'AuthController@home')->name('home');

//Auth::routes();