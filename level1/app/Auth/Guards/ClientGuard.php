<?php

namespace App\Auth\Guards;

use App\Client;
use Illuminate\Auth\SessionGuard as BaseGuard;

class ClientGuard extends BaseGuard {
    // override check : normal check + provider check + expireDate check : no need for remember token ?
    // override attempt in order to add social login
    // override logout

    public function check() {
        $loggedIn = parent::check();
        if(! $loggedIn) {
            return $loggedIn;
        }

        if(! ($this->user())->expireDate or is_null(($this->user())->expireDate)) {
            $this->logout();
            $loggedIn = false;
        }
        else {
            $now = new \DateTime();
            $expireDate = \DateTime::createFromFormat('Y-m-d H:i:s', ($this->user())->expireDate);
            $diff = $expireDate->diff($now);
            if(abs($diff->d) >= 15) {
                $this->logout();
                $loggedIn = false;
            }
        }

        return $loggedIn;
    }

    public function logout() {
        $user = $this->user();
        parent::logout();
        // extra cleanup
        $user->provider = null;
        $user->provider_id = null;
        $user->expireDate = null;
        $user->save();
    }

    public function attempt(array $credentials = [], $remember = false, $provider = 'email') {
        $attempt = false;
        if(! in_array($provider, resolve('auth-providers'))) {
            return false;
        }
        if($provider == 'email') {
            $attempt = parent::attempt($credentials, $remember);
        }
        else {
            if(! array_key_exists('provider', $credentials)
                or ! array_key_exists('provider_id', $credentials)
                or ! array_key_exists('email', $credentials)) {
                throw new \AssertionError('Attempting To Authenticate Without Valid Credentials');
            }
            $client = Client::where('email', $credentials['email'])->first();
            $client->provider = $credentials['provider'];
            $client->provider_id = $credentials['provider_id'];
            $client->expireDate = (new \DateTime())->format('Y-m-d H:i:s');
            if( ! $client->save() ) {
                return false;
            }
            $this->login($client);
            $attempt = true;
        }
        return $attempt;
    }
}