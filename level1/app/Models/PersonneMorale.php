<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonneMorale extends User
{
    protected $table = 'personne_physiques';
    protected $write_table = 'users';
    protected $read_table = 'personne_physiques';

    public function __constrcut()
    {
        parent::__construct();
        $this->write_table = User::getTable();
    }
}
