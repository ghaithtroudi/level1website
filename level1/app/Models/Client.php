<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends User
{

    protected $table = 'clients';
    protected $write_table = 'clients';
    protected $read_table = 'clients';

    protected $guarded = ['id', 'password'];

    protected $hidden = ['created_at', 'updated_at', 'provider_id', 'expireDate', 'type'];

    function reservations() {
        return $this->hasMany(\App\Reservation::class, 'client_id', 'id');
    }
}
