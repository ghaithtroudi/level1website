<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fablab extends Model
{
    protected $table = 'fablabs';

    function reservations() {
        return $this->belongsToMany(\App\Reservation::class, 'reservations_fablabs',
            'fablab_id', 'reservation_id');
    }
}
