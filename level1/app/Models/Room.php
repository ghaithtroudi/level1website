<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    function reservations() {
        return $this->belongsToMany(\App\Reservation::class, 'reservations_rooms',
            'room_id', 'reservation_id');
    }
}
