<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User  as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $write_table = 'users';
    protected $read_table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'nom', 'prenom', 'email', 'password',
//    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    protected function changeToWriteMode()
    {
        $this->table = $this->write_table;
    }

    protected function changeToReadMode()
    {
        $this->table = $this->read_table;
    }

    protected function hasManyModes()
    {
        return $this->write_table !== $this->read_table;
    }


    public function save(array $options = [])
    {
        if($this->hasManyModes()) {
            $this->changeToWriteMode();
            $saved = parent::save($options);
            $this->changeToReadMode();
            return $saved;
        }

        else {
            return parent::save($options);
        }
    }
}
