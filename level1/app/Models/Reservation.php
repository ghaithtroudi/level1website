<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';

    function client() {
        return $this->belongsTo(\App\Client::class, 'client_id', 'id');
    }

    function rooms() {
        return $this->belongsToMany(\App\Room::class, 'reservations_rooms',
                'reservation_id', 'room_id');
    }

    function fablabs() {
        return $this->belongsToMany(\App\Fablab::class, 'reservations_fablabs',
            'reservation_id', 'fablab_id');
    }
}
