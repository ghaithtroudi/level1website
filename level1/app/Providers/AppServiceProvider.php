<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        Validator::extend('human_name', function ($attribute, $value, $parameters, $validator) {

            $value_sanitised = preg_replace("/\s{2,}/", " ", $value);
            if (!preg_match("/^[a-zA-Z]+(\s[A-Za-z]+)*$/", $value_sanitised)) {
                return false;
            }

            return true;
        });

        Validator::extend('word', function ($attribute, $value, $parameters, $validator) {

            if ($value && !preg_match("/^[a-zA-Z]+([\s_\-\.\/]?[A-Za-z0-9]+)*$/", $value)) {
                return false;
            }

            return true;
        });

        Validator::extend('tel', function ($attribute, $value, $parameters, $validator) {

            $tel = preg_replace("/,/", "", $value);
            if ($tel && !preg_match("/^(\+)?[0-9]{8,}$/", $tel)) {
                return false;
            }
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('auth-providers', function () {
            return ['email', 'google', 'facebook'];
        });

        $this->app->bind('client-types', function () {
            return ['Individual', 'Company/Association'];
        });

        $this->app->bind('reservation_types', function() {
            return ['Normal', 'Event'];
        });
    }
}
