<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

use App\Client;


class AuthController extends Controller
{
    protected static $login_error = 'Make sure your enter your credentials correctly';

    public function getLogin() {
        return view('auth/login');
    }

    public function getRegister() {
        return view('auth/register');
    }

    public function login(Request $request, $provider) {
        if(! in_array($provider, resolve('auth-providers'))) {
            abort(404, 'Page Not Found');
        }

        if($provider == 'email') {

            $this->validate($request, [
                'email' => 'required|email|exists:clients,email',
                'password' => 'required',
            ]);

            $attempt = \Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);
            if(! $attempt) {
                return redirect()->route('getClientLogin')->withErrors(['login' => AuthController::$login_error]);
            }
            else {
                return redirect()->route('index');
            }
        }
        else {
            return Socialite::driver($provider)->redirect();
        }
    }

    public function register(Request $request, $provider) {
        if(! in_array($provider, resolve('auth-providers'))) {
            abort(404, 'Page Not Found');
        }


        if($provider == 'email') {
            $this->validate($request, [
                'email' => 'required|email|unique:clients,email',
                'password' => 'required|min:8|confirmed',
                'tel' => 'required|tel',
                'address' => 'sometimes|max:150',
                'name' => 'sometimes|human_name|max:50|min:3',
                'lastname' => 'sometimes|human_name|max:50|min:3',
                'company_name' => 'sometimes|max:50|word',
                'matricule' => 'sometimes',
                'cin' => 'sometimes|digits:8',
                'birthday' => 'sometimes|date_format:Y-m-d|before:today',
                'occupation' => 'sometimes|word',
                'type' => ['in' => resolve('client-types')],

            ]);

            $client = new Client($request->all());
            $client->password = password_hash($request->get('password'), CRYPT_BLOWFISH);
            $client->provider = 'email';
            if(! $client->save()) {
                return redirect()->route('getClientRegister')
                    ->withErrors(['register' => 'An error happened during registration, please try again']);
            }

            return redirect()->route('getClientLogin')->withSuccess('You have been successfully registered');
        }
        else {
            return Socialite::driver($provider)->redirect();
        }
    }

    public function loginCallback($provider) {
        if(! in_array($provider, resolve('auth-providers'))) {
            abort(404, 'Page Not Found');
        }

        $user = Socialite::driver($provider)->user();
        $client = $this->findOrCreateClient($user, $provider);
        $attempt = \Auth::attempt(['email' => $client->email, 'provider' => $provider, 'provider_id' => $user->getId()],
            true, $provider);

        if(! $attempt) {
            return redirect()->route('getClientLogin')->withErrors(['login' => AuthController::$login_error]);
        }

        return redirect()->route('index');
    }

    public function findOrCreateClient($user) {
        $client = Client::where('email', $user->getEmail())->first();
        if( is_null($client) or ! $client) {
            $client = new Client([
                'email' => $user->getEmail(),
            ]);
            $client->save();
        }

        return $client;
    }

    public function logout() {
        if(\Auth::check()) {
            \Auth::logout();
        }

        return redirect()->route('getClientLogin');
    }

    public function home() {
        return view('home');
    }
}
