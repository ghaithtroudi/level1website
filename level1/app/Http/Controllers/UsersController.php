<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
     $users =  User::all();
     // dd($users);
     return view('admin.users', compact('users'));
    }
    public function show()
    {
    }

    public function find(User $user)
    {
        return view('admin.userprofile',compact('user'));
    }

    public function layout()
    {
        return view('admin.layout');
    }
}
