<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>LEVEL1 INNOVATION HUB</title>
    <meta name="description" content="LEVEL1 is a Coworking Space situated in Lac 1 where you can come to work or study at any time without any reservation and you pay the time you spend in our spaces while enjoying unlimited and free coffee / Juice / Tea / Snacks !">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <meta name="keywords" content="Level1, Level one, lac, tunis, coworking space Tunis,coworking space Tunisia,coworking space Tunisie, LEVEL 1,salle de réunion tunis,espace évènement Tunis,anticafé tunisie,fablab tunisie,VR tunisie,AR tunisie,scanner 3d tunisie,3d tunisie,imprimante 3D tunisie,htc vive tunisie,oculus rift tunisie,location bureau tunisie,hololens tunisie,gaming tunisie,">

    <!-- Google fonts link-->
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!--For Plugins external css-->
    <link rel="stylesheet" href="css/plugins.css" />

    <!--Theme custom css -->
    <link rel="stylesheet" href="css/style.css">

    <!-- LightWidget WIDGET -->
    <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="css/responsive.css" />
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

</head>

<body data-spy="scroll" data-target=".navbar-collapse">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class='preloader'>
        <div class='loaded'>&nbsp;</div>
    </div>
    <div class="culmn">
        <header id="main_menu" class="header navbar-fixed-top">
            <div class="main_menu_bg">
                <div class="container">
                    <div class="row">
                        <div class="nave_menu">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse"
                                            aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#home" style="padding-top : 0px;">
                                            <img src="images/logo.png"/>
                                        </a>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->



                                    <div class="collapse navbar-collapse">

                                        <ul class="nav navbar-nav navbar-right">
                                            <li>
                                                <a href="#home">Home</a>
                                            </li>
                                            <li>
                                                <a href="#about"> About us</a>
                                            </li>
                                            <li>
                                                <a href="#service">Services</a>
                                            </li>
                                            <li>
                                                <a href="#pricing">Pricing</a>
                                            </li>
                                            <li>
                                                <a href="#activities">Activities</a>
                                            </li>
                                            <li>
                                                <a href="#gallery">Gallery</a>
                                            </li>
                                            <li>
                                                <a href="#team">Team</a>
                                            </li>
                                            <li>
                                                <a href="#contact">Contact</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </nav>
                        </div>
                    </div>

                </div>

            </div>
        </header>
        <!--End of header -->


        <!-- CAROUSEL -->
        <section id="home">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="images/homebg.jpg" alt="LEVEL1 Innovation Hub">
                        <div class="carousel-overlay"></div>
                        <div class="carousel-caption center-caption">
                            <h1 class="head">LEVEL1</h1>
                            <h2>Increase your productivity</h2>
                        </div>
                    </div>

                    <div class="item">
                        <img src="images/homebg2.jpg" alt="LEVEL1 Innovation Hub">
                        <div class="carousel-overlay"></div>
                        <div class="carousel-caption center-caption">
                            <h1 class="head">LEVEL1</h1>
                            <h2>Increase your productivity</h2>
                        </div>
                    </div>

                    <div class="item">
                        <img src="images/homebg3.jpg" alt="LEVEL1 Innovation Hub">
                        <div class="carousel-overlay"></div>
                        <div class="carousel-caption center-caption">
                            <h1 class="head">LEVEL1</h1>
                            <h2>Increase your productivity</h2>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- END CAROUSEL -->

        <section id="about" class="about">
            <div class="container">
                <div class="row">

                    <div class="main_about_area sections text-center">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="head_title text-center">
                                <h2>Concept</h2>
                                <div class="separator"></div>
                                <p>LEVEL1 is a Anti-Café/Coworking Space situated in Lac 1 (Tunis) that was opened for public in September 2017 and that brought a new style of coworking to the country: You come to work whenever you want without any reservation and you pay the time you spend in our spaces while enjoying unlimited and free coffee / Juice / Tea / Snacks!</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- SERVICES -->
        <section id="service" class="service" style="background: #f7f7f7;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="main_service_area sections">
                            <div class="row">
                                <div class="head_title text-center">
                                    <h2>Enjoy our services</h2>
                                    <div class="separator"></div>
                                    <p>Either you visit us for only one hour or for the whole day, you will beneficiate from
                                        all the following services</p>
                                </div>

                                <div class="main_service_content">
                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/wifi.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Free Wifi
                                            </h5>
                                            <h3>
                                                Optical Fiber
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/self-service.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Self Service
                                            </h5>
                                            <h3>
                                                Coffee | Juice | Snacks
                                            </h3>
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/office-chair.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Orthopedic Chair
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="border_devider"></div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="myservice">
                                                <span class="col-sm-12 nopadding">
                                                    <img src="fonts/svg/printer.svg" class="svg"></img>
                                                </span>
                                                <h5 class="col-sm-12 nopadding">
                                                    Photocopy
                                                </h5>
                                                <h3>
                                                    Black & Color
                                                </h3>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="myservice">
                                                <span class="col-sm-12 nopadding">
                                                    <img src="fonts/svg/scanner.svg" class="svg"></img>
                                                </span>
                                                <h5 class="col-sm-12 nopadding">
                                                    Scanner
                                                </h5>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="myservice">
                                                <span class="col-sm-12 nopadding">
                                                    <img src="fonts/svg/air-conditioner.svg" class="svg"></img>
                                                </span>
                                                <h5 class="col-sm-12 nopadding">
                                                    Air Conditioner
                                                </h5>
                                                <h3>
                                                    Hot | Cold
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border_devider"></div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/augmented-reality.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                AR/VR equipment
                                            </h5>
                                            <h3>
                                                ( Paid service )
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/drawer.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Private Locker
                                            </h5>
                                            <h3>
                                                ( Paid service )
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/no-smoking.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Smoke-Free Space
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="border_devider"></div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/group.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Community
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/home.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Legal Address
                                            </h5>
                                            <h3>70 HT-HRàS / Month</h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/meeting.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                2 Meeting Rooms
                                            </h5>
                                            <h3>
                                                Up to 8 persons
                                            </h3>
                                        </div>
                                    </div>


                                    <div class="border_devider"></div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/conference.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Conference Room
                                            </h5>
                                            <h3>
                                                Up to 60 persons
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/workshop.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Worksohp Room
                                            </h5>
                                            <h3>
                                                Up to 30 persons
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/video-conference.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Web Conference Room
                                            </h5>
                                            <h3>
                                                Fully Equiped
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="border_devider"></div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/conference.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                VR Games
                                            </h5>
                                            <h3>
                                                For Only 5 TD
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/parking.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Parking
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="myservice">
                                            <span class="col-sm-12 nopadding">
                                                <img src="fonts/svg/skype.svg" class="svg"></img>
                                            </span>
                                            <h5 class="col-sm-12 nopadding">
                                                Skype Box
                                            </h5>
                                            <h3>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END SERVICES -->

        <!-- Pricing Packs -->
        <section id="pricing">
            <div class="container">
                <div class="row">
                    <div class="sections">
                        <div class="head_title text-center">
                            <h2>Discover our Packs</h2>
                            <div class="separator"></div>
                        </div>
                        <div class="row">
                            <div class="text-center">
                                <h2>Regular</h2>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>5 DT / Hour</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                GlassHour
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                Are you looking for an accessible place where you can make a skype call, write an email or meet someone without being bothered
                                                by noise ? Join us at any hour from 08:00 to 20:00 from Monday to Sunday.
                                                We provide everything you need to get things done. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="text-center">
                                    <div class="pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>15 DT / 6 Hours</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                PitStop
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                If you found that spending just one hour or two is not enough and enjoyed your journey with us at LEVEL1, you can keep working
                                                for a whole 6 hours for only the price of 3 hours. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-0 col-sm-offset-3 col-sm-6">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>25 DT / Day</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                Tourist
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                Do you need an inspiring place to work or study in Tunis ? Join us for the whole day from 08:00 to 20:00 from Monday to Sunday
                                                . We provide everything you need to get things done. You are also welcome
                                                to use our kitchen facilities to prepare your meal! </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="border_devider"></div>
                            <div class="text-center">
                                <h2>Buy Packs with 20% off !</h2>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>40 DT</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                x10 GlassHour
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                You can buy a pack of 50 hours to use whenever you want with 20% discount! Book it as soon as possible.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>120 DT</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                x10 PitStop
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                You can buy a pack of 60 hours (10 *6 hours a day) to use whenever you want with 20% discount! Book it as soon as possible.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-0 col-sm-offset-3 col-sm-6">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>200 DT</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                x10 Tourist
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                You can buy a pack of 120 hours to use whenever you want with 20% discount! Book it as soon as possible.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border_devider"></div>
                            <div class="text-center">
                                <h2>Welcome Home</h2>
                            </div>
                            <div class="col-sm-6 col-md-4 col-md-offset-1">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>240 DT / Month</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                FLEX : 24 Tourist
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                You want Level 1 to be your office but you don't work during weekends? No worries, we thought about you! You can have access
                                                to the space 24h a day from Monday to Friday!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-md-offset-2">
                                <div class="text-center">
                                    <div class="card pricing_card">
                                        <div class="pricing_price col-xs-12">
                                            <h5>350 DT / Month</h5>
                                        </div>
                                        <div class="col-xs-12 pricing_header">
                                            <span class="pricing_header_content">
                                                Home : 24/7
                                            </span>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="pricing_content">
                                                You are free to use the space 24/7 and you are welcome to choose Level 1 as your office. You will be an important part of
                                                the community and you will get priviliged invitations to our events. Use
                                                it as if you were home!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Pricing Packs -->

        <div class="parallax"></div>

        <!-- What we do -->

        <section id="activities" class="doservice" style="background: #f7f7f7;">
            <div class="row">
                <div class="main_doservice sections">
                    <div class="head_title text-center">
                        <h2>what we do</h2>
                        <div class="separator"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="single_doservice">
                            <div class="single_doservice_img">
                                <img src="images/anticafe.jpg" alt="Activities" id="activitiesImage" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="single_doservice">
                            <div class="single_doservice_acording">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"
                                                    id="anticafeActivity">
                                                    <i class="fas fa-glass-martini" style="color: #f6c23a;"></i> Anticafé / Coworking
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                Either you are an entrepreneur, a freelancer, a consultant or a student you can come to work or to study at any time, without
                                                a reservation and for the duration you want, even for one hour, while beneficiating
                                                from free access to Wifi, drinks, snacks, printer etc. We are characterized
                                                by our gorgeous view of the Lake of Tunis, which will only inspire you to
                                                work harder and in a better atmosphere.
                                                <br/> In addition to that, there are 2 meetings rooms with a capacity of 4 to
                                                8 people that you can use for the same coworking price.
                                                <br/> We are open 7/7 from 8 am to 8 pm.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                                    id="fablabActivity" aria-controls="collapseTwo">
                                                    <i class="fab fa-nintendo-switch" style="color: #f6c23a;"></i> Fablab
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                Either you are a geek, a gamer , an entrepreneur or simply someone who is passionate about 3D, Augmented Realtity and Virtual
                                                Reality, we have a wide choice of high tech material you can use to build
                                                your products such as: Oculus Rift, HTC Vive, Microsoft Hololens, motion
                                                & leap & face capture, 3D scanner, 3D printer, engraving machine etc.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                                    id="eventsActivity" aria-controls="collapseThree">
                                                    <i class="fa fa-calendar" style="color: #f6c23a;"></i> Events
                                                    <i class="fa fa-chevron-down"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                            <div class="panel-body">
                                                Level 1 has an equipped conference room with a capacity of 50 people. We often organize or host trainings/events that target
                                                different segments such as students, entrepreneurs, civil society etc.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="clinetslogo" class="clinetslogo">
            <div class="containter">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="row">
                            <div class="head_title text-center">
                                <br/>
                                <h2>They trust us</h2>
                                <div class="separator"></div>
                            </div>
                            <section class="customer-logos slider">
                                <div class="slide">
                                    <a href="http://www.intilaq.tn/">
                                        <img src="images/intilaq.png" alt="Logo Intilaq">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://impactpartner.tn/fr/">
                                        <img src="images/impact-partner.png" alt="Logo Impact Partner">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="http://www.tunisianstartups.org/">
                                        <img src="images/tunisianstartups.png" alt="Logo Tunisian Startups">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://www.sofrecom.com/fr/">
                                        <img src="images/sofrecom.png">
                                    </a>
                                </div>
                                <div class="slide">
                                    <a href="https://wikistartup.wordpress.com/"></a>
                                    <img src="images/wikistartup.png" alt="Logo Wiki Startup">
                                </div>
                                <div class="slide">
                                    <a href="http://www.conect.org.tn"></a>
                                    <img src="http://www.conect.org.tn/sites/default/files/logo-conect-anim%C3%A9.gif" alt="Logo Conect">
                                </div>
                                <div class="slide">
                                    <a href="http://www.auf.org"></a>
                                    <img src="https://www.auf.org/wp-content/themes/auf/images/logo.jpg" alt="">
                                </div>
                                <div class="slide">
                                    <a href="https://www.afd.fr"></a>
                                    <img src="https://www.afd.fr/sites/afd/files/styles/1120_x_750/public/2017-11/afd-logo.jpg" alt="Logo de l'Agence Française de Développement">
                                </div>
                                <div class="slide">
                                    <a href="https://www.expertisefrance.fr/"></a>
                                    <img src="https://pbs.twimg.com/profile_images/908632105722241025/h3Xr1EUg.jpg" alt="Logo Expertise France">
                                </div>
                                <div class="slide">
                                    <a href="https://www.giz.de/en/worldwide/22600.html"></a>
                                    <img src="https://www.agripreneur.tn/fr/wp-content/uploads/2018/02/GIZ-TUNISIE-945x466.jpg" alt="Logo de GIZ Tunisie">
                                </div>
                                <div class="slide">
                                    <a href="http://www.tunisieindustrie.nat.tn/fr/home.asp"></a>
                                    <img src="https://africanmanager.com/wp-content/uploads/2017/02/APII.jpg" alt="Logo de GIZ Tunisie">
                                </div>

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="gallery" class="portfolio">
            <div class="container-fluid">
                <div class="row">
                    <div class="main_portfolio sections">
                        <div class="head_title text-center">
                            <h2>Gallery</h2>
                            <div class="separator"></div>
                        </div>

                        <div class="main_portfolio_content text-center">
                            <iframe src="http://lightwidget.com/widgets/96d0b065161e59a4a6392439ab511bff.html" scrolling="no" allowtransparency="true"
                                class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>

    <!-- TEST -->
    <section id="client" class="sections" style="background: #f7f7f7;">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="sections">
                        <div class="head_title text-center">
                            <h2>What people say</h2>
                            <div class="separator"></div>
                        </div>
                        <div class="reviews slider">
                            <div class="slide">
                                <div class="row">
                                            <div class="col-xs-4">
                                                <img class="img-circle review_image pull-right" src="https://scontent.ftun3-1.fna.fbcdn.net/v/t1.0-9/30707127_10156045408574550_8147106915273408512_n.jpg?_nc_cat=0&oh=86043baed53925f5b78e1b43443e28be&oe=5B80C8DA"
                                                    alt="" />
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="review_name">Raouf Ben Aissa</div>
                                                <p class="review_job">Sales Director</p>
                                                <span class="separator4"></span>
                                                <p class="review_text">The place to be :) Un acceuil chaleureux, un personnel professionnel et une
                                                    communauté diversifiée .Le tout avec la meilleure technologie du moment
                                                    :<br>Un vrai paradis pour tout startuppeur qui veut développer son projet
                                                    dans les meilleures conditions.</p>
                                            </div>
                                </div>
                            </div>

                            <div class="slide">
                                <div class="row">
                                            <div class="col-xs-4">
                                                <img class="img-circle review_image pull-right" src="https://scontent.ftun3-1.fna.fbcdn.net/v/t1.0-1/27459738_1735259573200878_4298062653172437678_n.jpg?_nc_cat=0&oh=8cdc12a736105b34dcaadd52ca31f171&oe=5BB29EC2"
                                                    alt="" />
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="review_name">Fahd Abidi</div>
                                                <p class="review_job">Social Media Specialist</p>
                                                <span class="separator4"></span>
                                                <p class="review_text">Awesome concept a great place to work , My favorite :)
                                                    <br> It's a second home</p>
                                    </div>
                                </div>
                            </div>

                            <div class="slide">
                                <div class="row">
                                            <div class="col-xs-4">
                                                <img class="img-circle review_image pull-right" src="https://scontent.ftun3-1.fna.fbcdn.net/v/t1.0-9/22089612_1593505624004076_4520818861371637890_n.jpg?_nc_cat=0&oh=2ee7111ba5c1533e3dc676fb9f2863e7&oe=5B797332"
                                                    alt="" />
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="review_name">Aymen Zaza</div>
                                                <p class="review_job">Engineer</p>
                                                <span class="separator4"></span>
                                                <p class="review_text">Great coworkers and a great place for young entrepreneurs.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="slide">
                                <div class="row">
                                            <div class="col-xs-4">
                                                <img class="img-circle review_image pull-right" src="https://scontent.ftun3-1.fna.fbcdn.net/v/t1.0-9/33785679_2017329504958273_5991072631632166912_n.jpg?_nc_cat=0&oh=8a8b6773f8ce586699df5604e8093372&oe=5B7E9428"
                                                    alt="" />
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="review_name">Malek Omri</div>
                                                <p class="review_job">Student, Social Activist</p>
                                                <span class="separator4"></span>
                                                <p class="review_text">Great place to work,study and meet the best people ever!!!!
                                                    <br>I recommend!</p>
                                    </div>
                                </div>
                            </div>

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section id="team" class="team">
        <div class="container">
            <div class="row">
                <div class="main_team sections">
                    <div class="head_title text-center">
                        <h2>Meet the team</h2>
                        <div class="separator"></div>
                    </div>

                    <div class="main_team_content text-center">
                        <div class="col-sm-4">
                            <div class="single_team">
                                <div class="single_team_img">
                                    <img src="images/riadhteam.jpg" alt="" />
                                    <div class="single_team_overlay all_overlay">
                                        <a href="https://www.facebook.com/ryadh.bouslama" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a> -->
                                        <a href="https://www.linkedin.com/in/ryadh-bouslama-60691023/" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a> -->
                                    </div>
                                </div>
                                <div class="single_team_content">
                                    <h5>Ryadh BOUSLAMA</h5>
                                    <p class="robotolight">CEO / Founder</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="single_team">
                                <div class="single_team_img">
                                    <img src="images/nada.jpg" alt="" />
                                    <div class="single_team_overlay all_overlay">
                                        <a href="https://www.facebook.com/nada.zaafrane" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a> -->
                                        <a href="https://www.linkedin.com/in/nada-zaafrane-957681111/" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a> -->
                                    </div>
                                </div>
                                <div class="single_team_content">
                                    <h5>Nada ZAAFRANE</h5>
                                    <p class="robotolight">Sales Manager</p>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="single_team">
                                <div class="single_team_img">
                                    <img src="images/ghaith.jpg" alt="" />
                                    <div class="single_team_overlay all_overlay">
                                        <a href="https://www.facebook.com/troudighaith" target="_blank">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a> -->
                                        <a href="https://www.linkedin.com/in/ghaithtoudi/" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <!-- <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a> -->
                                    </div>
                                </div>
                                <div class="single_team_content">
                                    <h5>Ghaith TROUDI</h5>
                                    <p class="robotolight">Fablab Manager</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="mymap">
        <div class="row">
            <div class="col-sm-12 nopadding">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3193.4770569023894!2d10.223370415242417!3d36.831049279942214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12fd3519c496d4d3%3A0x80f3289b8fdbecd6!2sLevel+1!5e0!3m2!1sfr!2stn!4v1528088561123"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>


    <section id="contact" style="background: #363636; color:#f7f7f7">
        <div class="container">
            <div class="row">
                <div class="sections">
                    <div class="head_title text-center">
                        <h2 style="color:white">Contact</h2>
                        <div class="separator"></div>
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <i class="fas fa-map-marker" style="color:#f6c23a;"></i>
                        &nbsp;Boulevard Cheikh Zaid,
                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les berges du Lac, 1053
                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tunis
                        <br>
                        <br>
                        <i class="far fa-envelope" style="color:#f6c23a;"></i>
                        &nbsp;contact@l1h.co
                        <br>
                        <br>
                        <i class="fas fa-phone" style="color:#f6c23a;"></i>
                        &nbsp;70 037 520
                    </div>
                    <div class="col-md-4">
                        Subscribe to our newsletter :
                        <br>
                        <br>
                        <div class="form-group">
                            <label class="sr-only" for="email">Amount (in dollars)</label>
                            <div class="input-group">
                                <div class="input-group-addon" id="newsletterbutton">
                                    <i class="far fa-envelope"></i>
                                </div>
                                <input type="email" class="form-control" id="email" placeholder="Your Email" style="color:black">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-4 text-center" style="font-size: 2em;">
                        <a href="https://www.facebook.com/level1hub" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="https://www.instagram.com/levelonehub/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/level1hub/" target="_blank">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </div>
                </div>
            </div>
    </section>

    <section id="contact" class="footer_widget">
        <div class="container">
            <div class="row">
                <div class="main_widget">
                    <div class="col-xs-12">
                        <div class="row">
                            <!-- FACEBOOK -->
                            <div class="col-md-6  col-xs-12">
                                <div style="height: 32px">
                                    <span class="monseratregular">Facebook</span>
                                    <span class="pull-right">
                                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FLevel1Hub%2F&width=94&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId"
                                            width="94" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                                            allowTransparency="true" allow="encrypted-media"></iframe>
                                    </span>
                                </div>
                                <div class="hidden-sm hidden-xs">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLevel1Hub%2F&tabs=timeline&width=500&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                                    width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0"></iframe>
                                </div>
                                </div>
                            <!-- INSTAGRAM -->
                            <div class="col-md-6 col-xs-12">
                                <a href="https://www.instagram.com/levelonehub/" target="_blank">
                                    <span class="pull-right insta"> Follow US :
                                        <span class="fa fa-instagram"></span>
                                    </span>
                                </a>
                                <p class="monseratregular">Instagram</p>
                                <iframe src="http://lightwidget.com/widgets/7dbac17fda925c5d82d5f689384ef9e6.html" scrolling="no" allowtransparency="true"
                                    class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
    </section>




    <footer id="footer" class="footer">
        <div class="container">
            <div class="main_footer text-center">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="copyright_text">
                            <p class=" wow fadeInRight" data-wow-duration="1s">Made with
                                <i class="fa fa-heart"></i> by
                                <a href="http://www.pyntopyn.com">PYNTOPYN TECHNOLOGIES</a>
                                Theme by
                                <a href="http://bootstrapthemes.co">Bootstrap Themes</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    </div>

    <!-- START SCROLL TO TOP  -->

    <div class="scrollup">
        <a href="#home">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
    <!-- JAVASCRIPT -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/script.js"></script>
    <!-- FIREBASE -->
    <script src="https://www.gstatic.com/firebasejs/5.0.4/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-database.js"></script>

    <script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCxj8IdB8oKpsDWWaHjPR8OYu6J7PmvHRg",
        authDomain: "level1-newsletter.firebaseapp.com",
        databaseURL: "https://level1-newsletter.firebaseio.com",
        projectId: "level1-newsletter",
        storageBucket: "",
        messagingSenderId: "911373468696"
    };
    firebase.initializeApp(config);
    </script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121179065-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-121179065-1');
    </script>

    <!-- <script src="js/plugins.js"></script> -->
    <script src="js/main.js"></script>

</body>

</html>