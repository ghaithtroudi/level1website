<div class="sidebar" data-image="assets/img/sidebar-lvl1.jpg" data-color="black">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                LEVEL 1 HUB
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item active">
                <a class="nav-link" href="dashboard.html">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="./space">
                    <i class="nc-icon nc-tv-2"></i>
                    <p>Spaces</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="./fablab">
                    <i class="nc-icon nc-ruler-pencil"></i>
                    <p>Fablab</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="./event">
                    <i class="nc-icon nc-album-2"></i>
                    <p>Events</p>
                </a>
            </li>
        </ul>
    </div>
</div>